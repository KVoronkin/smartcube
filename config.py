"""Конфигурация программного обеспечения."""
import configparser

CONFIG_FILE = 'config.ini'
config = configparser.ConfigParser()
config.read(CONFIG_FILE)

wkhtmltopdf_src = config.get('wkhtmltopdf', 'SRC')
DB_NAME = config.get('DATABASE', 'NAME')
DB_LOGIN = config.get('DATABASE', 'LOGIN')
DB_PSWD = config.get('DATABASE', 'PSWD')
DB_HOST = config.get('DATABASE', 'HOST')
SSH_ROOT = '127.0.0.1:8000'
db_type = config.get('DATABASE', 'TYPE')
DB_ENGINE = ''
if db_type == 'MySQL':
    DB_ENGINE = 'django.db.backends.mysql'
if db_type == 'SQLlite':
    DB_ENGINE = 'django.db.backends.sqlite3'

EMAIL_REVIEW_SUBJECT = 'Пользователь заполнил форму с отзывом на сайте!'
EMAIL_REVIEW_MESSAGE = """Имя пользователя: {0}
Телефон: {1}
Электронная почта: {2}
Текст отзыва: {3} 
Дата и время отправки: {4}
"""
EMAIL_VERIFY_SUBJECT = 'Подтверждение авторизации на сайте "Умный куб"'
EMAIL_VERIFY_MESSAGE = """
Попытка входа в систему требует дополнительной проверки, введите проверочный код указанный в письме.

Пользователь {0}
Проверочный код: {1}

Если вы не пытались войти в свою учетную запись, ваш пароль может быть скомпрометирован. 
"""