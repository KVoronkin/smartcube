

$(document).ready(function () {
    let add_cart = document.querySelectorAll('.cart-checker')
    let checkArr = []
    let total_price = 0
    for (let e of add_cart.values()) {
        total_price += parseFloat(e.attributes.getNamedItem("price").value);
        const p = {
            id: e.attributes.getNamedItem("id").value,
            checked: true,
            price: e.attributes.getNamedItem("price").value,
            count: 1
        }

        $('#total_price').text(total_price + ' руб.')
        checkArr.push(p)
        e.addEventListener("change", (e) => {
            const idx = checkArr.findIndex((el) => el.id === e.target.id)
            checkArr[idx].checked = e.target.checked
            total_price = 0
            for (let elem of checkArr){
                if (elem.checked){
                    total_price += parseFloat(elem.price) * elem.count
                }
            }
            $('#total_price').text(total_price + ' руб.')
        })

    }

    $('.minus').click(function () {
        total_price = 0;
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        let temp_cash = $(this).parent().parent().parent().parent().children('.summa').children('.cash-price')
        temp_cash.text(temp_cash.attr('value') * $input.val() + ' руб.')
        const idplus =  checkArr.findIndex((el) => el.id.slice(13) === $(this).attr('id').slice(11))
        checkArr[idplus].count = $input.val();
        for (let item of checkArr){
            if (item.checked){
                total_price += item.price * item.count;
            }
        }
        $('#total_price').text(total_price+ ' руб.')
        return false;
    });
    $('.plus').click(function () {
        total_price = 0;
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        let temp_cash = $(this).parent().parent().parent().parent().children('.summa').children('.cash-price')
        temp_cash.text(temp_cash.attr('value') * $input.val() + ' руб.')
        const idplus =  checkArr.findIndex((el) => el.id.slice(13) === $(this).attr('id').slice(10))
        checkArr[idplus].count = $input.val();
        for (let item of checkArr){
            if (item.checked){
                total_price += item.price * item.count;
            }
        }
        $('#total_price').text(total_price+ ' руб.')
        return false;
    });
$('.order_cart').on('click', function() {
    window.location = '/my/cart/order'
})
});