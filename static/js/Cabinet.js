
let mychart = document.getElementById('chart').getContext('2d');
let chart = NaN;
draw_history = function(label, today_noize_history, normar_level) {
    chart = new Chart(mychart, {
        type: 'line',
        data: {
            labels: label,
            datasets: [
                {
                    label: "Превышение",
                    backgroundColor: 'rgba(255, 99, 132, 0.7)',
                    borderColor: 'rgb(255,99,132)',
                    data: today_noize_history,
                    type: 'bar',
                },
                {
                label: "Нормальное значение",
                backgroundColor: 'rgba(63, 244, 123, 0.3)',
                borderColor: 'rgb(63,244,123)',
                data: normar_level,
            }]
    
        },
        options: {
            scales: {
                yAxes: [{
                    stacked: true
                }]
            }
        }
    });
}

$(document).ready(function () {
    $('#cube_list_choise').hide();
    $('select').formSelect();
    $('.modal').modal();
    $('.tabs').tabs();

    $('.add-cube').on('click', function () {
        $('.sub_info_cube').css('display', 'none');
        $('.addable-card').slideToggle();
    })
    $('.close-card').on('click', function () {
        $('.addable-card').slideToggle();
    })
    // Добавление куба к аккаунту
    $('.register_cube').on('click', function () {
        fetch('/api/user/cube', {
            method: "POST",
            body: JSON.stringify({
                short_uid: $('#uid_cube').val()
            })
        }).then(function (response) {
            return response.json()
        }).then(function (data) {
            if (data.success) {
                $('.sub_info_cube').slideToggle();
                $('#cube_list_choise').css('display', 'flex');
                $('#cube_list_choise').append(`
            <div style="display: flex; flex-direction: column; align-items: center; margin: 0 5%;  position: relative;">
                <a href="?cube=`+ data.cube_id + `" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></a>
                <img src="/static/img/icon1.png" style="width: 60px; height: 60px;" alt="">
                <h6 style="font-size: 12px; white-space: nowrap;">Куб №`+ data.cube_id + `</h6>
            </div>
            `);
                $('.register_cube_put').attr('id', data.cube_id)
            } else {
                alert(data.error)
            }
        })

    })
    // Опциональные настройки куба
    $('.register_cube_put').on('click', function () {
        fetch('/api/cube', {
            method: "PUT",
            body: JSON.stringify({
                cube_id: $(this).attr('id'),
                name: $('#cube_name').val(),
                description: $('#adress_cube').val()
            })
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data.success) {
                alert('Куб успешно добавлен')
            } else {
                alert(data.error)
            }
        })
    })
    device_types = {
        '0': 'Датчик шума',
        '1': 'Датчик температуры',
        '2': 'Датчик влажности',
        '3': 'Датчик движения',
        '4': 'Датчик открытия',
        '6': 'Умная лампочка',
        '7': 'Умные шторы',
    }
    // Привязка устройства к кубу
    $('.bind_device').on('click', function () {
        let cubeId = $(this).attr('id')
        fetch('/api/cube/device', {
            method: "POST",
            body: JSON.stringify({
                cube_id: cubeId,
                short_uid: $('#short_pass').val()
            })
        }).then(function (response) {
            return response.json()
        }).then(function (data) {
            if (data.success) {
                // Быстрое добавление в список без обновления страницы
                fetch('/api/cube/device?device_id=' + data.id + '&cube_id=' + cubeId)
                    .then(function (response) {
                        return response.json()
                    })
                    .then(function () {
                        window.location.reload();
                    })
            }
            else {
                alert(data.error)
            }
        })
    })
    $('.get_device_info').on('click', function () {
        let temp_cube_id = $(this).attr('data-cube').slice(12)
        let temp_device_id = $(this).attr('id')
        var date_start_hist = new Date($.now())
        date_start_hist = date_start_hist.getFullYear() + '-' +
            ("0" +(date_start_hist.getMonth()+1)).slice(-2) + '-' +
            ("0" + date_start_hist.getDate()).slice(-2)
        var date_end_hist = new Date($.now())
        date_end_hist.setDate(date_end_hist.getDate() + 1)
        date_end_hist = date_end_hist.getFullYear() + '-' +
            ("0" + (date_end_hist.getMonth()+1)).slice(-2) + '-' +
            ("0" + date_end_hist.getDate()).slice(-2)
        $('.date_start_hist').val(date_start_hist)
        $('.date_end_hist').val(date_end_hist)

        // Получение полной информации по устройству
        fetch('/api/cube/device?device_id=' + temp_device_id + '&cube_id=' + temp_cube_id)
            .then(function (response) {
                return response.json()
            }).then(function (data) {
                if (data.success) {

                    $('#modal' + data['info'].id)
                        .children('.modal-content')
                        .children('.meta_info')
                        .children('.col')
                        .children('.input-field')
                        .children('#device_description')
                        .val(data['info'].description)
                    var now = new Date($.now());
                    var datestart = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate() + 'T00:00:00'
                    var dateend = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + (now.getDate() + 1) + 'T00:00:00'
                    var threshold_value = data['info'].threshold_value
                    // Получение графика истории значений датчика
                    fetch('/api/cube/device/history', {
                        method: "POST",
                        body: JSON.stringify({
                            cube_id: temp_cube_id,
                            device_id: temp_device_id,
                            datetime_start: datestart,
                            datetime_end: dateend
                        })
                    }).then(function (response) {
                        return response.json()
                    }).then(function (data) {
                        if (data.success) {
                            if (data.history.length != 0){
                                $('.circle-data div').html(data.history[data.history.length - 1].value)
                            }else{
                                $('.circle-data div').html(40)
                            }
                            
                            let labels = []
                            let today_noize_history = []
                            let normar_level = [threshold_value]
                            let history = data.history;
                            for (let i = 0; i < history.length; i++){
                                labels.push(history[i]['date']);
                                today_noize_history.push(history[i]['value']);
                                normar_level.push(threshold_value);
                            }                        
                            draw_history(labels, today_noize_history, normar_level);
                            let temp_history_content = $('#history_cont_' + temp_device_id).parent().parent().children('#history_contain')
                            temp_history_content.css('display', 'unset')
                        } else {
                            alert(data.error)
                        }
                    })

                    if (data['info'].threshold_value) {
                        $('#modal' + data['info'].id)
                            .children('.modal-content')
                            .children('.meta_info')
                            .children('.col')
                            .children('.input-field')
                            .children('#threshold_value')
                            .val(data['info'].threshold_value)
                            

                    } else {
                        $('#modal' + data['info'].id)
                            .children('.modal-content')
                            .children('.meta_info')
                            .children('.input-field')
                            .children('#threshold_value')
                            .parent()
                            .css('display', 'none')
                    }


                } else {
                    alert(data.error)
                }
            })
    })
    let date_shifts = {
        'today': [0, 0, 0, 1],
        'yesterday': [0, 0, 1, 0],
        'week': [0, 0, 7, 1],
        'month': [0, 1, 0, 1],
        'year': [1, 0, 0, 1],
    }
    // Быстрые вкладки с выводом информации по измерениям датчика да определенное время
    $('.history_tabs').on('click', function () {
        var datestart = new Date($.now());
        var dateend = new Date($.now());
        var history_cube_id = $(this).parent().parent().parent().attr('id')
        var history_device_id = $(this).parent().parent().attr('id').substr(13)
        datestart.setDate(datestart.getDate() - date_shifts[$(this).attr('id')][2])
        datestart.setMonth(datestart.getMonth() - date_shifts[$(this).attr('id')][1])
        datestart.setFullYear(datestart.getFullYear() - date_shifts[$(this).attr('id')][0])
        datestart = datestart.getFullYear() +
            '-' + (datestart.getMonth() + 1) +
            '-' + datestart.getDate() + 'T00:00:00';

        dateend.setDate(dateend.getDate() + date_shifts[$(this).attr('id')][3])
        dateend = dateend.getFullYear() +
            '-' + (dateend.getMonth() + 1) +
            '-' + dateend.getDate() + 'T00:00:00';

        fetch('/api/cube/device/history', {
            method: "POST",
            body: JSON.stringify({
                cube_id: history_cube_id,
                device_id: history_device_id,
                datetime_start: datestart,
                datetime_end: dateend
            })
        }).then(function (response) {
            return response.json()
        }).then(function (data) {

            if (data.success) {
                let labels = []
                let today_noize_history = []
                let normar_level = [threshold_value.value]
                let history = data.history;
                for (let i = 0; i < history.length; i++){
                    labels.push(history[i]['date']);
                    today_noize_history.push(history[i]['value']);
                    normar_level.push(threshold_value.value);
                }                        
                chart.destroy();
                draw_history(labels, today_noize_history, normar_level);
                $('.circle-data div').html(history[history.length - 1].value)
                $('.changed_tabs_content_' + history_device_id).css('display', 'unset')
            } else {
                alert(data.error)
            }
        })

    })
    // Создание отчета по данным устройства пользователя
    $('.report_create').on('click', function () {
        var datestart_field = $(this).parent().children('.date_history').children('.date_start_hist').val()
        var dateend_field = $(this).parent().children('.date_history').children('.date_end_hist').val()
        fetch('/api/cube/noise_sensor/report', {
            method: "POST",
            body: JSON.stringify({
                cube_id: $(this).attr('data-cube'),
                device_id: $(this).attr('data-device'),
                date_start: datestart_field + 'T00:00:00',
                date_end: dateend_field + 'T00:00:00'
            })
        }).then(function(response){
            if(response.status == '200'){
                return response.blob()
            }else{
                return response.json()
            }
        }).then(function(data){
            if(data.type == 'application/pdf'){
                var file = new Blob([data], { type: 'application/pdf;base64' });
                var fileURL = URL.createObjectURL(file);
                var link = document.createElement('a')
                link.setAttribute('href', fileURL)
                link.setAttribute('download', 'Отчет_датчика_шума.pdf')
                link.click()
                link.remove()
            }
            else{
                alert(data.error)
            }
        })
    })
    $('.show_cube_list').on('click', function () {
        $('#cube_list_choise').toggle('slide');
    })
});