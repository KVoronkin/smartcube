let reg = $('#regconfirm');
reg.click(function(){
    if($('#pwd1').val() != $('#pwd2').val()){
        $('#pwd-error').css("display", "flex");
    }else{
        $('#pwd-error').css("display", "none");
    }
    fetch('/api/auth/registration', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            username: $('#login').val(),
            first_name: $('#fname').val(),
            last_name: $('#lname').val(),
            password: $('#pwd1').val(),
            email: $('#email').val()
        })
}).then(function(response){
    return response.json();
}).then(function(data){
    if(data.success){
        location = '/';
    }else{
        alert('Регистрация не удалась' + data.error)
    }
})
})