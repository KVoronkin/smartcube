$(document).ready(function () {
    $('select').formSelect();
    $('.modal').modal();

});
let cash_cubes_info = []
let device_enum = {
    'device-0': 'Датчик шума',
    'device-1': 'Датчик температуры',
    'device-2': 'Датчик влажности',
    'device-3': 'Датчик движения',
    'device-4': 'Датчик открытия',
}
let side_list = ['first_edge', 'second_edge', 'third_edge', 'fourth_edge', 'fifth_edge']

// Функция логики программы в зависимости от вкладки настроек
var tabParser = function (temp_cube) {
    let value = $('.cube_radio:checked').attr('id');
    let tab = (window.location.search).slice(5)
    if (tab == 'Cubes' || tab == '') {
        // Вкладка настроек куба
        if (temp_cube) {
            // Переназначение имени и описания
            $('#cube_name').val(temp_cube.name)
            $('#textarea_desc').val(temp_cube.custom_description)
            // Переназначение списка доверенных пользователей
            let temp_users = ''
            for (let subuser of temp_cube.users) {
                temp_users += `<li style="position:relative; font-size: 18px; line-height: 38px; padding-bottom: 2%; border-bottom: 1px solid aliceblue;"><i class="material-icons left" style="font-size: 3rem;">account_circle</i>` + subuser + `
                    <a id="`+ subuser + `" class="btn-floating btn-small waves-effect waves-light red unbind_user_cube" style="position:absolute; top: 0; right: 0;"><i class="material-icons">delete</i></a>
                    </li>`

                $('.subuser_cube#cubes-' + value).html(temp_users + `
                <li style="line-height: 34px; display: flex; width: 100%; margin-top: 2%;"><a class="btn-floating btn waves-effect waves-light red cube_user_add" style="margin-right:3%;"><i class="material-icons";>add</i></a>Добавить пользователя</li>
                `)
                // Удаление доверенного пользователя
                $('.unbind_user_cube').on('click', function () {
                    fetch('/api/cube/users', {
                        method: "DELETE",
                        body: JSON.stringify({
                            cube_id: value,
                            username: $(this).attr('id')
                        })
                    }).then(function (response) {
                        return response.json();
                    }).then(function (data) {
                        if (data.success) {
                            alert('Удаление пользователя прошло успешно.')
                            window.location.reload();
                        } else {
                            alert(data.error)
                        }
                    })
                })
            }
        }
        $('.cube_user_add').on('click', function () {
            $('.add_cube_user_block').slideToggle();
        })
    } else if (tab == 'Sides') {
        // Вкладка настроек граней и кнопок устройста
        fetch('/api/cube/devices_info?id=' + temp_cube.id)
            .then(function (response) {
                return response.json();
            }).then(function (data) {
                if (data.success) {
                    let connected_device = {}
                    connected_device['button-' + temp_cube.first_edge.id] = temp_cube.first_edge.button_info.device_info.id;
                    connected_device['button-' + temp_cube.second_edge.id] = temp_cube.second_edge.button_info.device_info.id;
                    connected_device['button-' + temp_cube.third_edge.id] = temp_cube.third_edge.button_info.device_info.id;
                    connected_device['button-' + temp_cube.fourth_edge.id] = temp_cube.fourth_edge.button_info.device_info.id;
                    connected_device['button-' + temp_cube.fifth_edge.id] = temp_cube.fifth_edge.button_info.device_info.id;
                    
                    let temp_select_device_button = ''
                    let temp_select_device_edge = ''
                    let edge_device = []
                    // Типы устройств привязываемых к граням
                    fetch('/api/cube/edge/device_types')
                    .then(function(response){
                        return response.json()
                    })
                    .then(function(data){
                        if(data.success){
                            edge_device = data.device_types
                        }
                    })
                    for (key of Object.keys(data.device_info)) {

                        for (let concret_device of (data.device_info)[key]) {
                            if(concret_device.device_type in edge_device){
                                temp_select_device_edge += '<option value="' + concret_device.id + '">' + device_enum['device-' + concret_device.device_type] + ' ' + concret_device.description + '</option>'
                            }
                            temp_select_device_button += '<option value="' + concret_device.id + '">' + device_enum['device-' + concret_device.device_type] + ' ' + concret_device.description + '</option>'
                        }
                    }
                    temp_select_device_edge = '<option value="" disabled selected>Выберите устройство</option>' + temp_select_device_edge;
                    temp_select_device_button = '<option value="" disabled selected>Выберите устройство</option>' + temp_select_device_button;

                    // Запонение формы выбора привязываемых устройств
                    $('select').formSelect();
                    let temp_side = ''
                    for (let num_side of side_list) {
                        temp_side += `
                        <div id="`+ temp_cube.id + `" class="col s12" style="display: flex; align-items: center; border-bottom: 1px solid #878b96; margin-bottom: 2%;">
                        <div style="width: 40px; height: 40px; background-color: `+ temp_cube[num_side].color + `; border-radius: 4px;">
                        </div>
                            <div id="cont-edge-`+ temp_cube[num_side].id + `" class="col s12 edge_contain">
                            <div class="col s12 edges" style="margin-bottom: 2%;">
                                <div class="col s3" >
                                   <h6 class="truncate" style="margin: 0; line-height: 40px;">Грань куба:</h6>
                                </div>
                                <div class="input-field col s5" style="margin: 0;">
                                <select style='display:block;'>`+ temp_select_device_edge + `</select>
                                </div>
                                <div class="col s4" >
                                    <a class="waves-effect waves-light btn edge_bind_cube">Применить</a>
                                </div>
                            </div>
                            <div class="col s12 edges_buttons">
                                <div class="col s3" >
                                   <h6 class="truncate" style="margin: 0; line-height: 40px;">Кнопка куба:</h6>
                                </div>
                                <div class="input-field col s5" style="margin: 0;">
                                <select style='display:block;'>`+ temp_select_device_button + `</select>
                                </div>
                                <div class="col s4" >
                                    <a class="waves-effect waves-light btn btn_bind_cube">Применить</a>
                                </div>
                            </div>
                            </div>
                        </div>`
                    }

                    $('.cube_sides').html(temp_side);
                    for (const [key, value] of Object.entries(connected_device)) {  
                        $('.edge_contain#cont-edge-' + key.slice(7))
                            .children('.edges_buttons')
                            .children('.input-field')
                            .children('select').val(value) 
                    }
                    // Привязка устройства к грани куба
                    $('.edge_bind_cube').on('click', function () {
                        let temp_edge_id = $(this).parent().parent().parent().attr('id')
                        fetch('/api/cube/edge', {
                            method: "PUT",
                            body: JSON.stringify({
                                cube_id: $(this).parent().parent().parent().parent().attr('id'),
                                edge_id: temp_edge_id.substr(10),
                                device_id: $(this).parent().parent().children('.input-field').children('select').val()
                            })
                        }).then(function (response) {
                            return response.json();
                        }).then(function (data) {
                            if (data.success) {
                                alert('Привяка выполнена успешно')
                                window.location.reload();
                            } else {
                                alert(data.error)
                            }
                        })
                    })
                    // Привязка устройства к кнопке куба
                    $('.btn_bind_cube').on('click', function () {
                        let temp_edge_id = $(this).parent().parent().parent().attr('id')
                        fetch('/api/cube/edge/button', {
                            method: "PUT",
                            body: JSON.stringify({
                                cube_id: $(this).parent().parent().parent().parent().attr('id'),
                                edge_id: temp_edge_id.substr(10),
                                device_id: $(this).parent().parent().children('.input-field').children('select').val()
                            })
                        }).then(function (response) {
                            return response.json();
                        }).then(function (data) {
                            if (data.success) {
                                alert('Привяка выполнена успешно')
                                window.location.reload();
                            } else {
                                alert(data.error)
                            }
                        })
                    })
                }
            })
    }
}
// Функция сбора информации по кубам пользвателя
var getCube = function () {
    let value = $('.cube_radio:checked').attr('id');
    let key = 'cube-' + value;
    temp_cube = cash_cubes_info[key]
    $('.add_cube_user').attr('id', value);
    $('.put_changes_cube').attr('id', value);
    $('.subuser_cube').attr('id', 'cubes-' + value)
    if (temp_cube == undefined) {
        // Кеширование результатов полной информации о кубе
        fetch('/api/cube?id=' + value)
            .then(function (response) {
                return response.json()
            }).then(function (data) {
                if (data.success) {
                    cash_cubes_info[key] = data.cube_info
                    temp_cube = data.cube_info
                    $('.cube_name').val(temp_cube.name)
                    $('.textarea_desc').val(temp_cube.custom_description)
                    return temp_cube
                } else {
                    return false
                }
            }).then(function (temp_cube) {
                tabParser(temp_cube);
            }
            )

    } else {
        tabParser(temp_cube);  
    }
}

// Изменение опциональных настроек профиля пользователя
$('#user_update').on('click', function () {
    fetch('/api/user', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            email: $('#email').val(),
            first_name: $('#first_name').val(),
            last_name: $('#second_name').val()
        })
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        if (data.success) {
            alert('Изменения приняты')
        }
        else {
            alert(data.error);
        }
    })
})
// Изменение пароля пользователя
$('#pwd_change_btn').on('click', function () {
    fetch('/api/user/change_password', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            old_password: $('#past_pwd').val(),
            new_password_first: $('#new_pwd').val(),
            new_password_second: $('#confirm_pwd').val()
        })
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        if (data.success) {
            alert('Изменения приняты')
            window.location.reload();
        }
        else {
            alert(data.error);
        }
    })
})
$(document).on('ready', function () {
    getCube()
})

$('.cube_radio').on('change', function () {
    getCube()
})
// Изменение опциональных настроек куба
$('.put_changes_cube').on('click', function () {
    fetch('/api/cube', {
        method: "PUT",
        body: JSON.stringify({
            cube_id: $(this).attr('id'),
            name: $(this).parent().parent().children('.input-field').children('#cube_name').val(),
            description: $(this).parent().parent().children('.input-field').children('#textarea_desc').val()
        })
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        if (data.success) {
            alert('Изменения вступили в силу')
        } else {
            alert(data.error)
        }
    })
})
// Добавление доверенного пользователя
$('.add_cube_user').on('click', function () {
    let new_user = $(this).parent().parent().children('.input-field').children('#cube_user_login').val()
    fetch('/api/cube/users', {
        method: "POST",
        body: JSON.stringify({
            cube_id: $(this).attr('id'),
            username: new_user,
            password: $(this).parent().parent().children('.input-field').children('#cube_user_password').val()
        })
    })
        .then(function (response) {
            return response.json()
        }).then(function (data) {
            if (data.success) {
                alert('Пользователь добавлен')
            } else {
                alert('Добавление не удалось')
            }
        })
})
// Включение/Отключение функции двухфакторной аутентификации
$('#change_verify_status').on('click',function(){
    fetch('/api/user/verify', {
        method: "PUT",
        body: JSON.stringify({
            is_verify: $('#verify_status').is(':checked')
        })
    }).then(function(response){
        return response.json()
    }).then(function(data){
        if(data.success){
            alert('Данные успешно изменены')
            window.location.reload()
        }else{
            alert('Что-то пошло не так')
        }
    })
})