// Подключение визуальных элементов библиотеки materialize
$(document).ready(function(){
    $('.parallax').parallax();
  });

  $(document).ready(function(){
    $('.scrollspy').scrollSpy();
  });

  $(document).ready(function(){
    $('.tabs').tabs();
  });
  $(document).ready(function(){
    $('.collapsible').collapsible();
  });