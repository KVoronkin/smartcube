$('.send_feedback').on('click',function(){
    fetch('/api/send_mail/review', {
        method: "POST",
        body: JSON.stringify({
            user_name: $('#user_name').val(),
            user_mail:  $('#user_mail').val(),
            user_phone: $('#user_phone').val(),
            message:  $('#message').val()
        })
    }).then(function(response){
        return response.json();
    }).then(function(data){
        if(data.success){
            alert('Ваш отзыв успешно отправлен разработчикам');
        }else{
            alert('Что-то пошло не так, '+data.error);
        }
    })
})