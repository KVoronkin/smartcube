"""Модуль URL адресов для приложения backend."""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from .api import email_req, auth_req, user_req, cube_req, device_req, edge_req, noise_sensor_req, user_verify

urlpatterns = [
      path('send_mail/review', email_req.review_form, name='review_mail'),
      path('auth/registration', auth_req.registration_req, name='registration'),
      path('auth/get_token', auth_req.get_token, name='get_token'),
      path('user', user_req.user_req, name='user_req'),
      path('user/change_password', user_req.change_password, name='change_password'),
      path('user/cube', user_req.users_cube, name='users_devices'),
      path('user/confirm', user_verify.send_verify_message, name='double_auth'),
      path('user/verify', user_verify.confirm_verify, name='double_auth_check'),
      path('cube', cube_req.cube_req, name='cube_req'),
      path('cube/devices_info', cube_req.device_req, name='cube_req'),
      path('cube/users', cube_req.cube_users, name='cube_users'),
      path('cube/device', device_req.devices_req, name='device_req'),
      path('cube/device/history', device_req.history_req, name='device_history'),
      path('cube/edge', edge_req.edge_req, name='edge_req'),
      path('cube/edge/<str:action>', edge_req.edge_req, name='edge_req'),
      path('cube/noise_sensor/report', noise_sensor_req.report_req, name='edge_req'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
