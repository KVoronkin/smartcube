import json
from django.http import HttpResponse
from .json_handler import DatetimeEncoder

def get_response(is_json: bool, http_status: int, success: bool, error_message: str, **kwargs):
    """Сформировать ответ сервера.

    Args:
        is_json (bool): Сформировать ответ в формате json.
        http_status (int): Статус ответа.
        success (bool): Разрешен ли доступ.
        error_message (str): Сообщение об ошибке.

    Returns:
        Сформированный ответ сервера.
    """
    content = dict(**{'success': success, 'error': error_message}, **kwargs)
    if is_json:
        resp = HttpResponse(content_type='application/json')
        resp.status_code = http_status
        resp.content = json.dumps(content, cls=DatetimeEncoder)
    else:
        resp = HttpResponse()
        resp.status_code = http_status
        resp.content = content
    return resp