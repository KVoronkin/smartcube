from functools import wraps

from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core import exceptions

from .request_serialize import get_response


def check_authorization():
    """Декоратор для проверки авторизации.

    Args:
        correct_params: Параметры, которые должны присутствовать в запросе.
    """

    def decorator(view):
        """Стандарт Django.

        Args:
            view: Функция, обрабатывающая запрос.

        Returns:
            Результат обработки декоратора.
        """

        @wraps(view)
        def wrapper(request, *args, **kwargs):
            """Функция проверка авторизации.

            Args:
                request: Данные поступившего запроса.

            Returns:
                Результат проверки.
            """
            try:
                token = request.headers.get('Authorization')
                if not token:
                    token = request.COOKIES.get('sessionid')
                session = Session.objects.get(session_key=token)
                uid = session.get_decoded().get('_auth_user_id')
                user = User.objects.get(pk=uid)
            except exceptions.ObjectDoesNotExist:
                return get_response(True, 401, False, 'Unauthorized')
            return view(
                request,
                *args,
                **kwargs,
                user=user,
            )

        return wrapper

    return decorator
