import pdfkit
from django.http import HttpResponse
from django.template.loader import get_template

from config import wkhtmltopdf_src


def render_to_pdf(template_src, context_dict, name_file):
    """Вернуть pdf файл для загрузки из html файла.

    @param template_src: Путь до html файла.
    @param context_dict: Словарь контекстных переменных.
    @param name_file: Название конечного файла pdf.
    @return: response в формате application/pdf.
    """
    options = {
        'encoding': "UTF-8",
        'enable-local-file-access': None,
    }
    config = pdfkit.configuration(wkhtmltopdf=wkhtmltopdf_src)
    template = get_template(template_src)
    html = template.render(context_dict)
    pdf = pdfkit.from_string(html, False, configuration=config, options=options)
    response = HttpResponse(pdf, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="{0}.pdf"'.format(name_file)
    return response
