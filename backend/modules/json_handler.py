"""Модули формирования ответов JSON."""
import json

class DatetimeEncoder(json.JSONEncoder):
    """Распаковка даты и времени."""

    def default(self, obj):  # pylint: disable=E0202
        """Данные типа datetime.

        Args:
            obj (datetime): Время.

        Returns:
            (str): Время в строковом представлении.
        """
        try:
            return super(DatetimeEncoder, obj).default(obj)
        except TypeError:
            return obj.strftime("%d.%m.%Y %H:%M:%S")

