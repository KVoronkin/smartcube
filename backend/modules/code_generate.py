"""Модуль генерации кодов активации."""
import string
from random import choice, randint
import uuid

def generate_uid():
    """Метод генерации uid.

    Returns:
        str: Строка, содержащая сгенерированный пароль.
    """
    return str(uuid.uuid4())
