"""Модуль тестирования запросов связанных с авторизацией."""
import json
from backend.models import SmartLamp
from django.test import Client, TestCase
from backend.tests.modules import init_test_data, create_cube


class EdgeReqTest(TestCase):
    """Класс тестирования запросов связанных с устройствами."""

    @classmethod
    def setUpTestData(cls):
        cls.test_data = init_test_data()

    def setUp(self):
        self.client = Client()
        self.client.login(username='testuser', password='12345678')
        add_data = {'uid': self.test_data['cube'].uid}
        self.client.post('/api/user/cube', add_data, 'application/json', )

    def test_add_device_to_button(self):
        noise_sensor = self.test_data['noise_sensor']
        cube = self.test_data['cube']
        noise_sensor.cube = cube
        noise_sensor.save()

        req_data = {
            'cube_id': cube.id,
            'device_id': noise_sensor.id,
            'edge_id': cube.first_edge.id,
        }
        response = self.client.put('/api/cube/edge/button', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)

        req_data['cube_id'] = -1
        response = self.client.put('/api/cube/edge/button', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = cube.id
        req_data['device_id'] = -1
        response = self.client.put('/api/cube/edge/button', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['device_id'] = noise_sensor.id
        req_data['edge_id'] = -1
        response = self.client.put('/api/cube/edge/button', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

    def test_add_device_to_edge(self):
        noise_sensor = self.test_data['noise_sensor']
        cube = self.test_data['cube']
        noise_sensor.cube = cube
        noise_sensor.save()
        smart_lamp = SmartLamp.objects.create(
            uid='qwerty-87654321-87654321',
            short_uid='qwerty',
            cube=cube,
        )
        smart_lamp.save()

        req_data = {
            'cube_id': cube.id,
            'device_id': smart_lamp.id,
            'edge_id': cube.first_edge.id,
        }
        response = self.client.put('/api/cube/edge', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)

        req_data['device_id'] = noise_sensor.id
        response = self.client.put('/api/cube/edge', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        new_cube = create_cube('newcub')
        req_data['device_id'] = smart_lamp.id
        req_data['edge_id'] = new_cube.first_edge.id
        response = self.client.put('/api/cube/edge', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

    def test_get_edge_device_types(self):
        response = self.client.get('/api/cube/edge/device_types')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/api/cube/edge/bad')
        self.assertEqual(response.status_code, 404)

    def test_no_method(self):
        """Проверка корректного ответа на запрос с отствующем методом."""
        response = self.client.head('/api/cube/edge')
        self.assertEqual(response.status_code, 405)
