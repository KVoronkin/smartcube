"""Модуль тестирования запросов связанных с авторизацией."""
import json

from django.test import Client, TestCase

from backend.tests.modules import init_test_data, create_user


class CubeReqTest(TestCase):
    """Класс тестирования запросов связанных с кубами."""

    @classmethod
    def setUpTestData(cls):
        cls.test_data = init_test_data()

    def setUp(self):
        self.client = Client()
        self.client.login(username='testuser', password='12345678')
        add_data = {'uid': self.test_data['cube'].uid}
        self.client.post('/api/user/cube', add_data, 'application/json', )

    def test_get_cube_info(self):
        """Тестирование запроса получение информации о кубе."""
        response = self.client.get('/api/cube?id=' + str(self.test_data['cube'].id))
        self.assertEqual(response.status_code, 200)
        resp_data = json.loads(response.content)
        self.assertEqual(resp_data['cube_info']['id'], self.test_data['cube'].id)

        response = self.client.get('/api/cube?id=-1')
        self.assertEqual(response.status_code, 400)

    def test_update_cube_info(self):
        """Тестирование обновление информации о кубе"""
        req_data = {
            'cube_id': self.test_data['cube'].id,
            'name': 'NewCubName',
            'description': 'New description',
        }
        response = self.client.put('/api/cube', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/api/cube?id=' + str(self.test_data['cube'].id))
        resp_data = json.loads(response.content)
        self.assertEqual(resp_data['cube_info']['name'], req_data['name'])
        self.assertEqual(resp_data['cube_info']['custom_description'], req_data['description'])

        req_data['name'] = ''
        response = self.client.put('/api/cube', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)
        req_data['name'] = 'newName'
        req_data['cube_id'] = -1
        response = self.client.put('/api/cube', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

    def test_get_cub_device_info(self):
        """Тестирование получения информации о датчиках куба."""
        noise_sensor = self.test_data['noise_sensor']
        cube = self.test_data['cube']
        noise_sensor.cube = cube
        noise_sensor.save()

        response = self.client.get('/api/cube/devices_info?id=' + str(cube.id))
        self.assertEqual(response.status_code, 200)
        resp_data = json.loads(response.content)
        self.assertTrue(resp_data['device_info']['noise_sensor'])
        self.assertEqual(resp_data['device_info']['noise_sensor'][0]['id'], noise_sensor.id)

        response = self.client.get('/api/cube/devices_info?id=-1')
        self.assertEqual(response.status_code, 400)

    def test_add_new_user_to_cube(self):
        """Тестирование добавления новых пользователей для куба."""
        create_user('newuser')
        create_user('newuser2')
        req_data = {
            'cube_id': self.test_data['cube'].id,
            'username': 'newuser',
            'password': '12345678'
        }
        response = self.client.post('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = -1
        response = self.client.post('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = self.test_data['cube'].id
        req_data['password'] = 'badpwd'
        response = self.client.post('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 403)

        req_data['password'] = '12345678'
        req_data['username'] = 'badlogin'
        response = self.client.post('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        self.client.login(username='newuser', password='12345678')
        req_data['username'] = 'newuser2'
        response = self.client.post('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 403)

    def test_delete_cube_user(self):
        create_user('newuser')
        create_user('newuser2')
        req_data = {
            'cube_id': self.test_data['cube'].id,
            'username': 'newuser',
            'password': '12345678'
        }
        self.client.post('/api/cube/users', req_data, 'application/json')
        req_data['username'] = 'newuser2'
        self.client.post('/api/cube/users', req_data, 'application/json')
        req_data = {
            'cube_id': self.test_data['cube'].id,
            'username': 'newuser',
        }
        response = self.client.delete('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)
        response = self.client.delete('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = -1
        response = self.client.delete('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = self.test_data['cube'].id
        req_data['username'] = 'testuser'
        response = self.client.delete('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['username'] = 'badlogin'
        response = self.client.delete('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        self.client.login(username='newuser2', password='12345678')
        req_data['username'] = 'newuser2'
        response = self.client.delete('/api/cube/users', req_data, 'application/json')
        self.assertEqual(response.status_code, 403)

    def test_no_method(self):
        """Проверка корректного ответа на запрос с отствующем методом."""
        response = self.client.head('/api/cube')
        self.assertEqual(response.status_code, 405)
        response = self.client.head('/api/cube/devices_info')
        self.assertEqual(response.status_code, 405)
        response = self.client.head('/api/cube/users')
        self.assertEqual(response.status_code, 405)
