"""Модуль тестирования запросов связанных с авторизацией."""
import json

from django.test import Client, TestCase
from backend.models import NoiseSensorValues
from backend.tests.modules import init_test_data


class DeviceReqTest(TestCase):
    """Класс тестирования запросов связанных с устройствами."""

    def setUp(self):
        self.client = Client()

    def test_email_send(self):
        req_data = {
            'user_name': 'UserName',
            'user_mail': 'mail@mail.ru',
            'user_phone': 88005553535,
            'message': 'Some text',
        }
        response = self.client.post('/api/send_mail/review', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)

        req_data['user_phone'] = 'badphone'
        response = self.client.post('/api/send_mail/review', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['user_phone'] = 88005553535
        req_data['user_mail'] = 'badmail'
        response = self.client.post('/api/send_mail/review', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)