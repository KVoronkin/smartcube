"""Модуль с общими функциями для тестов."""

from django.contrib.auth.models import User

from backend.models import Cube, CubeEdge, EdgeButton, NoiseSensor, TemperatureSensor


def create_user(name: str):
    test_user = User.objects.create_user(
        name,
        'test@gmail.com',
        '12345678',
    )
    test_user.save()
    return test_user


def create_cube(name: str):
    first_button = EdgeButton.objects.create(device=None)
    second_button = EdgeButton.objects.create(device=None)
    third_button = EdgeButton.objects.create(device=None)
    fourth_button = EdgeButton.objects.create(device=None)
    fifth_button = EdgeButton.objects.create(device=None)
    first_button.save()
    second_button.save()
    third_button.save()
    fourth_button.save()
    fifth_button.save()
    first_edge = CubeEdge.objects.create(color='#000000', button=first_button, device=None)
    second_edge = CubeEdge.objects.create(color='#000000', button=second_button, device=None)
    third_edge = CubeEdge.objects.create(color='#000000', button=third_button, device=None)
    fourth_edge = CubeEdge.objects.create(color='#000000', button=fourth_button, device=None)
    fifth_edge = CubeEdge.objects.create(color='#000000', button=fifth_button, device=None)
    first_edge.save()
    second_edge.save()
    third_edge.save()
    fourth_edge.save()
    fifth_edge.save()
    test_cube = Cube.objects.create(
        name=name,
        uid='123456-12345678-12345678',
        short_uid='123456',
        first_edge=first_edge,
        second_edge=second_edge,
        third_edge=third_edge,
        fourth_edge=fourth_edge,
        fifth_edge=fifth_edge,
    )
    test_cube.save()
    return test_cube


def init_test_data():
    """Инициализация данных в базе для тестов."""
    test_user = create_user('testuser')
    test_cube = create_cube('TestCube')
    test_noise_sensor = NoiseSensor.objects.create(
        uid='123456-12345678-12345678',
        short_uid='123456',
        threshold_value=40,
    )
    test_noise_sensor.save()
    test_temp_sensor = TemperatureSensor.objects.create(
        uid='654321-87654321-87654321',
        short_uid='654321',
        min_value=-20,
        max_value=20,
    )
    test_temp_sensor.save()

    return {
        'user': test_user,
        'cube': test_cube,
        'noise_sensor': test_noise_sensor,
        'temp_sensor': test_temp_sensor,
    }
