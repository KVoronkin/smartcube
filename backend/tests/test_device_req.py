"""Модуль тестирования запросов связанных с авторизацией."""
import json

from django.test import Client, TestCase

from backend.models import NoiseSensorValues
from backend.tests.modules import init_test_data


class DeviceReqTest(TestCase):
    """Класс тестирования запросов связанных с устройствами."""

    @classmethod
    def setUpTestData(cls):
        cls.test_data = init_test_data()

    def setUp(self):
        self.client = Client()
        self.client.login(username='testuser', password='12345678')
        add_data = {'uid': self.test_data['cube'].uid}
        self.client.post('/api/user/cube', add_data, 'application/json', )

    def test_get_device_data(self):
        noise_sensor = self.test_data['noise_sensor']
        cube = self.test_data['cube']
        noise_sensor.cube = cube
        noise_sensor.save()
        response = self.client.get('/api/cube/device?cube_id={0}&device_id={1}'.format(
            str(cube.id),
            str(noise_sensor.id)
        )
        )
        self.assertEqual(response.status_code, 200)
        resp_data = json.loads(response.content)
        self.assertEqual(resp_data['info']['id'], noise_sensor.id)

        response = self.client.get('/api/cube/device?cube_id={0}&device_id={1}'.format(str(cube.id), '2'))
        self.assertEqual(response.status_code, 400)
        response = self.client.get('/api/cube/device?cube_id={0}&device_id={1}'.format('-1', str(noise_sensor.id)))
        self.assertEqual(response.status_code, 400)

    def test_add_device_to_cube(self):
        noise_sensor = self.test_data['noise_sensor']
        temp_sensor = self.test_data['temp_sensor']
        cube = self.test_data['cube']

        req_data = {
            'cube_id': cube.id,
            'uid': noise_sensor.uid,
        }
        response = self.client.post('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)
        req_data = {
            'cube_id': cube.id,
            'short_uid': temp_sensor.short_uid,
        }
        response = self.client.post('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['short_uid'] = 'baduid'
        response = self.client.post('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data = {
            'cube_id': cube.id,
            'uid': 'baduid'
        }
        response = self.client.post('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = -1
        response = self.client.post('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data = {'cube_id': cube.id}
        response = self.client.post('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_device_info(self):
        noise_sensor = self.test_data['noise_sensor']
        temp_sensor = self.test_data['temp_sensor']
        cube = self.test_data['cube']
        temp_sensor.cube = cube
        noise_sensor.cube = cube
        noise_sensor.save()
        temp_sensor.save()
        req_data = {
            'cube_id': cube.id,
            'device_id': noise_sensor.id,
            'description': 'New description',
            'threshold_value': 50,
        }
        response = self.client.put('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/api/cube/device?cube_id={0}&device_id={1}'.format(
            str(cube.id),
            str(noise_sensor.id)
        )
        )
        resp_data = json.loads(response.content)
        self.assertEqual(resp_data['info']['threshold_value'], req_data['threshold_value'])
        req_data = {
            'cube_id': cube.id,
            'device_id': temp_sensor.id,
            'description': 'New description',
            'min_value': -20,
            'max_value': 20
        }
        response = self.client.put('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)

        req_data['cube_id'] = -1
        response = self.client.put('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = cube.id
        req_data['device_id'] = -1
        response = self.client.put('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['device_id'] = noise_sensor.id
        req_data['threshold_value'] = 50
        response = self.client.put('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['device_id'] = temp_sensor.id
        response = self.client.put('/api/cube/device', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

    def test_get_device_history(self):
        noise_sensor = self.test_data['noise_sensor']
        noise_sensor.cube = self.test_data['cube']
        noise_sensor.save()
        value1 = NoiseSensorValues.objects.create(
            noise_sensor=noise_sensor,
            value=30,
            date='2021-01-01T00:00:00'
        )
        value2 = NoiseSensorValues.objects.create(
            noise_sensor=noise_sensor,
            value=40,
            date='2021-02-01T00:00:00'
        )
        value1.save()
        value2.save()
        req_data = {
            'cube_id': self.test_data['cube'].id,
            'device_id': noise_sensor.id,
            'datetime_start': '2021-01-01T00:00:00',
            'datetime_end': '2021-02-01T23:59:59'
        }
        response = self.client.post('/api/cube/device/history', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)
        resp_data = json.loads(response.content)
        self.assertEqual(len(resp_data['history']), 2)
        req_data['datetime_end'] = '2021-01-01T23:59:59'
        response = self.client.post('/api/cube/device/history', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)
        resp_data = json.loads(response.content)
        self.assertEqual(len(resp_data['history']), 1)

        req_data['cube_id'] = -1
        response = self.client.post('/api/cube/device/history', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = self.test_data['cube'].id
        req_data['device_id'] = -1
        response = self.client.post('/api/cube/device/history', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

    def test_no_method(self):
        """Проверка корректного ответа на запрос с отствующем методом."""
        response = self.client.head('/api/cube/device')
        self.assertEqual(response.status_code, 405)
        response = self.client.head('/api/cube/device/history')
        self.assertEqual(response.status_code, 405)
