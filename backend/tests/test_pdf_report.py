"""Модуль тестирования запросов связанных с авторизацией."""
import json

from django.test import Client, TestCase
from backend.models import NoiseSensorValues
from backend.tests.modules import init_test_data


class DeviceReqTest(TestCase):
    """Класс тестирования запросов связанных с устройствами."""

    @classmethod
    def setUpTestData(cls):
        cls.test_data = init_test_data()

    def setUp(self):
        self.client = Client()
        self.client.login(username='testuser', password='12345678')
        add_data = {'uid': self.test_data['cube'].uid}
        self.client.post('/api/user/cube', add_data, 'application/json', )

    def test_pdf_report(self):
        noise_sensor = self.test_data['noise_sensor']
        cube = self.test_data['cube']
        noise_sensor.cube = cube
        noise_sensor.activation_date = '2021-01-01T00:00:00'
        noise_sensor.save()
        value1 = NoiseSensorValues.objects.create(
            noise_sensor=noise_sensor,
            value=70,
            date='2021-01-01T00:00:00'
        )
        value2 = NoiseSensorValues.objects.create(
            noise_sensor=noise_sensor,
            value=100,
            date='2021-02-01T00:00:00'
        )
        value1.save()
        value2.save()

        req_data = {
            'cube_id': self.test_data['cube'].id,
            'device_id': noise_sensor.id,
            'date_start': '2021-01-01T00:00:00',
            'date_end': '2021-02-01T23:59:59'
        }
        response = self.client.post('/api/cube/noise_sensor/report', req_data, 'application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['content-type'], 'application/pdf')

        req_data['date_start'] = '2021-05-01T00:00:00'
        req_data['date_end'] = '2021-05-01T00:00:00'
        response = self.client.post('/api/cube/noise_sensor/report', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = -1
        response = self.client.post('/api/cube/noise_sensor/report', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

        req_data['cube_id'] = cube.id
        req_data['device_id'] = -1
        response = self.client.post('/api/cube/noise_sensor/report', req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

    def test_no_method(self):
        """Проверка корректного ответа на запрос с отствующем методом.
        """
        response = self.client.head('/api/cube/noise_sensor/report')
        self.assertEqual(response.status_code, 405)
