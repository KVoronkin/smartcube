"""Модуль тестирования запросов связанных с авторизацией."""
import json

from django.contrib.auth.models import User
from django.test import Client, TestCase
from backend.tests.modules import init_test_data

class AuthTest(TestCase):
    """Класс тестирования запросов связанных с авторизацией."""

    @classmethod
    def setUpTestData(cls):
        cls.test_data = init_test_data()

    def setUp(self):
        self.client = Client()
        self.test_req_data = {
            'username': 'user_login',
            'email': 'user@gmail.com',
            'password': 'userpassword1234',
            'first_name': 'Ivan',
            'last_name': 'Ivanov',
        }
        self.token_data = {
            'username': 'testuser',
            'password': '12345678',
        }

    def test_registration(self):
        """Тестирование регистрации нового пользователя.
        """
        response = self.client.post(
            '/api/auth/registration',
            self.test_req_data,
            'application/json',
        )
        self.assertEqual(response.status_code, 200)
        user = User.objects.filter(username=self.test_req_data['username'])
        self.assertTrue(user)
        response = self.client.post(
            '/api/auth/registration',
            self.test_req_data,
            'application/json',
        )
        self.assertEqual(response.status_code, 400)

    def test_reg_with_bad_data(self):
        """Тестирование регистрации нового пользователя c некорректными данными.
        """
        self.test_req_data['username'] = ''
        response = self.client.post(
            '/api/auth/registration',
            self.test_req_data,
            'application/json',
        )
        self.assertEqual(response.status_code, 400)

        self.test_req_data['username'] = 'user_login'
        self.test_req_data['email'] = ''
        response = self.client.post(
            '/api/auth/registration',
            self.test_req_data,
            'application/json'
        )
        self.assertEqual(response.status_code, 400)

        self.test_req_data['email'] = 'email'
        response = self.client.post(
            '/api/auth/registration',
            self.test_req_data,
            'application/json'
        )
        self.assertEqual(response.status_code, 400)

        self.test_req_data['password'] = ''
        response = self.client.post(
            '/api/auth/registration',
            self.test_req_data,
            'application/json'
        )
        self.assertEqual(response.status_code, 400)

    def test_get_token(self):
        response = self.client.post(
            '/api/auth/get_token',
            self.token_data,
            'application/json',
        )
        self.assertEqual(response.status_code, 200)
        resp_data = json.loads(response.content)
        self.assertTrue(resp_data['token'])

    def test_token_bad_data(self):
        self.token_data['username'] = ''
        response = self.client.post(
            '/api/auth/get_token',
            self.token_data,
            'application/json',
        )
        self.assertEqual(response.status_code, 400)
        self.token_data['username'] = 'testuser'
        self.token_data['password'] = 'badpassword'
        response = self.client.post(
            '/api/auth/get_token',
            self.token_data,
            'application/json',
        )
        self.assertEqual(response.status_code, 403)

    def test_no_method(self):
        """Проверка корректного ответа на запрос с отствующем методом.
        """
        response = self.client.head('/api/auth/registration')
        self.assertEqual(response.status_code, 405)
