"""Модуль тестирования запросов связанных с авторизацией."""
import json

from django.contrib.auth.models import User
from django.test import Client, TestCase

from backend.tests.modules import init_test_data


class UserReqTest(TestCase):
    """Класс тестирования запросов связанных с пользователями."""

    @classmethod
    def setUpTestData(cls):
        cls.test_data = init_test_data()

    def setUp(self):
        self.user_req_data = {
            'email': 'newmail@mail.ru',
            'first_name': 'Ivan',
            'last_name': 'Ivanov',
        }
        self.client = Client()
        self.client.login(username='testuser', password='12345678')

    def test_get_user_info(self):
        """Тестирование запроса получения информации о пользователе."""
        user = self.test_data['user']
        response = self.client.get('/api/user')
        resp_data = json.loads(response.content)
        self.assertEqual(resp_data['user_info']['username'], user.username)

    def test_update_user_info(self):
        """Тестирование обновления информации о пользователе."""
        response = self.client.put('/api/user', self.user_req_data, 'application/json')
        self.assertEqual(response.status_code, 200)
        user = User.objects.filter(username='testuser').first()
        self.assertEqual(user.first_name, self.user_req_data['first_name'])

    def test_update_user_info_bad_data(self):
        """Тестирование обновления информации о пользователе с невиладными данными."""
        self.user_req_data['email'] = 'badmail'
        response = self.client.put('/api/user', self.user_req_data, 'application/json')
        self.assertEqual(response.status_code, 400)

    def test_change_pwd(self):
        """Тестирование изменения пароля пользователя."""
        pwd_data = {
            'old_password': '12345678',
            'new_password_first': '87654321',
            'new_password_second': '87654321',
        }
        response = self.client.put('/api/user/change_password', pwd_data, 'application/json', )
        self.assertEqual(response.status_code, 200)
        response = self.client.put('/api/user/change_password', pwd_data, 'application/json', )
        self.assertEqual(response.status_code, 403)
        pwd_data['old_password'] = '87654321'
        pwd_data['new_password_first'] = '12345678'
        response = self.client.put('/api/user/change_password', pwd_data, 'application/json', )
        self.assertEqual(response.status_code, 400)
        pwd_data['new_password_first'] = 'bad'
        pwd_data['new_password_second'] = 'bad'
        response = self.client.put('/api/user/change_password', pwd_data, 'application/json', )
        self.assertEqual(response.status_code, 400)

    def test_user_cube_add(self):
        add_data = {'uid': self.test_data['cube'].uid}
        response = self.client.post('/api/user/cube', add_data, 'application/json', )
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/api/user/cube', add_data, 'application/json', )
        self.assertEqual(response.status_code, 400)
        add_data['uid'] = 'baduid'
        response = self.client.post('/api/user/cube', add_data, 'application/json', )
        self.assertEqual(response.status_code, 400)
        add_data = {}
        response = self.client.post('/api/user/cube', add_data, 'application/json', )
        self.assertEqual(response.status_code, 400)

    def test_get_users_cube_info(self):
        add_data = {'short_uid': self.test_data['cube'].short_uid}
        self.client.post('/api/user/cube', add_data, 'application/json', )
        response = self.client.get('/api/user/cube')
        resp_data = json.loads(response.content)
        self.assertTrue(resp_data['cube_info'])

    def test_no_method(self):
        """Проверка корректного ответа на запрос с отствующем методом.
        """
        response = self.client.head('/api/user/cube')
        self.assertEqual(response.status_code, 405)
        response = self.client.head('/api/user')
        self.assertEqual(response.status_code, 405)
        response = self.client.head('/api/user/change_password')
        self.assertEqual(response.status_code, 405)
