""""Модуль обработки запросов связанных с устройствами"""
from datetime import datetime
from typing import Optional

from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from pydantic import BaseModel, validator

from backend.enums.device_types import DeviceTypes
from backend.models import Device, Cube
from backend.modules.auth import check_authorization
from backend.modules.request_serialize import get_response


class AppendDeviceData(BaseModel):
    cube_id: int
    uid: Optional[str] = None
    short_uid: Optional[str] = None

    @validator('short_uid', always=True)
    def check_uid_or_short_uid(cls, short_uid, values):
        if not values['uid'] and not short_uid:
            raise ValueError('Ожидался uid или short_uid')
        return short_uid


class UpdateData(BaseModel):
    cube_id: int
    device_id: int
    description: Optional[str] = None
    threshold_value: Optional[int] = None
    min_value: Optional[int] = None
    max_value: Optional[int] = None


class HistoryData(BaseModel):
    cube_id: int
    device_id: int
    datetime_start: datetime
    datetime_end: datetime


@csrf_exempt
@check_authorization()
def devices_req(request, **kwargs):
    """Функция принятия стандартных запросов связанных с устройствами.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'GET':
        cube_id = request.GET.get('cube_id', -1)
        device_id = request.GET.get('device_id', -1)
        cube = Cube.objects.filter(pk=cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        try:
            device = Device.objects.get_subclass(pk=device_id, cube=cube)
        except ObjectDoesNotExist:
            return get_response(True, 400, False, 'Устройство не найдено')
        return get_response(True, 200, True, '', info=device.full_info())
    if request.method == 'POST':
        req_data = AppendDeviceData.parse_raw(request.body)
        cube = Cube.objects.filter(pk=req_data.cube_id, main_user=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        device = Device.objects.none()
        if req_data.short_uid:
            device = Device.objects.filter(
                short_uid=req_data.short_uid,
                activation_date=None,
            ).first()
        elif req_data.uid:
            device = Device.objects.filter(
                uid=req_data.uid,
                activation_date=None,
            ).first()
        if not device:
            return get_response(True, 400, False, 'Устройство не найдено!')
        device.cube = cube
        device.activation_date = timezone.now()
        device.save()
        return get_response(True, 200, True, '', id=device.pk)
    elif request.method == 'PUT':
        req_data = UpdateData.parse_raw(request.body)
        cube = Cube.objects.filter(pk=req_data.cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        try:
            device = Device.objects.get_subclass(pk=req_data.device_id, cube=cube)
        except ObjectDoesNotExist:
            return get_response(True, 400, False, 'Устройство не найдено')
        if req_data.description:
            device.custom_description = req_data.description
        if req_data.threshold_value:
            if device.device_type not in (DeviceTypes.device_with_threshold_value()):
                return get_response(True, 400, False, 'Невозможно обновить указанные значения у данного устройства.')
            device.threshold_value = req_data.threshold_value
        if req_data.min_value and req_data.max_value:
            if device.device_type != DeviceTypes.TEMPERATURE_SENSOR.value:
                return get_response(True, 400, False, 'Невозможно обновить указанные значения у данного устройства.')
            device.min_value = req_data.min_value
            device.max_value = req_data.max_value
        device.save()
        return get_response(True, 200, True, '', id=device.id)
    else:
        return get_response(True, 405, False, 'Метод отсутствует')


@csrf_exempt
@check_authorization()
def history_req(request, **kwargs):
    """Функция принятия стандартных запросов связанных с устройствами.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'POST':
        req_data = HistoryData.parse_raw(request.body)
        cube = Cube.objects.filter(pk=req_data.cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        try:
            device = Device.objects.get_subclass(pk=req_data.device_id, cube=cube)
        except ObjectDoesNotExist:
            return get_response(True, 400, False, 'Устройство не найдено')
        history = device.get_history(req_data.datetime_start, req_data.datetime_end)
        return get_response(True, 200, True, '', history=history)
    else:
        return get_response(True, 405, False, 'Метод отсутствует')
