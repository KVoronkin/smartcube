"""Модуль ообработки запросов связанных с регистрацией и авторизацией пользователей."""
from django.contrib.auth import login, update_session_auth_hash
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from pydantic import BaseModel, constr
from pydantic.class_validators import validator
from pydantic.networks import EmailStr

from backend.modules.errors import ERR_LIST
from backend.modules.request_serialize import get_response


class UserData(BaseModel):
    username: constr(min_length=1)
    email: EmailStr
    password: str
    first_name: str
    last_name: str

    @validator('password')
    def check_len_pwd(cls, password: str):
        if len(password) < 8:
            raise ValueError('Пароль должен быть не менее 8 символов.')
        return password


class AuthData(BaseModel):
    username: str
    password: str


@csrf_exempt
def registration_req(request):
    """Обработка стандартных запросов связанных с регистрацией.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    if request.method == 'POST':
        user_data = UserData.parse_raw(request.body)
        if User.objects.filter(username=user_data.username).exists():
            return get_response(True, 400, False, 'Пользователь с таким логином уже существует')
        user = User.objects.create_user(
            user_data.username,
            user_data.email,
            user_data.password,
        )
        user.first_name = user_data.first_name
        user.last_name = user_data.last_name
        user.save()
        login(request, user)
        return get_response(True, 200, True, '')
    else:
        return get_response(True, 405, False, ERR_LIST['no_method'])


@csrf_exempt
def get_token(request):
    """Обработка запроса создания сессии и получения токена для пользователя.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    if request.method == 'POST':
        req_data = AuthData.parse_raw(request.body)
        user = User.objects.filter(username=req_data.username).first()
        if not user:
            return get_response(True, 400, False, 'Пользователь с таким логином не существует.')
        if not user.check_password(req_data.password):
            return get_response(True, 403, False, 'Пароль неверен!')
        login(request, user)
        update_session_auth_hash(request, user)
        token = request.session.session_key
        return get_response(True, 200, True, '', token=token)
