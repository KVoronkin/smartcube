"""Модуль ообработки запросов связанных с пользователями."""
from typing import Optional

from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from pydantic import BaseModel, EmailStr, validator

from backend.models import Cube
from backend.modules.auth import check_authorization
from backend.modules.errors import ERR_LIST
from backend.modules.request_serialize import get_response


class UserNewData(BaseModel):
    first_name: str
    last_name: str
    email: EmailStr


class NewPwdData(BaseModel):
    old_password: str
    new_password_first: str
    new_password_second: str

    @validator('new_password_first')
    def check_pwd_len(cls, first_pwd):
        if len(first_pwd) < 8:
            raise ValueError('Пароль должен содержать не менее 8 символов!')
        return first_pwd

    @validator('new_password_second')
    def equal_pwds(cls, second_pwd, values):
        if second_pwd != values.get('new_password_first'):
            raise ValueError('Пароль должен содержать не менее 8 символов!')
        return second_pwd


class AppendCubeData(BaseModel):
    uid: Optional[str] = None
    short_uid: Optional[str] = None

    @validator('short_uid', always=True)
    def check_uid_or_short_uid(cls, short_uid, values):
        if not values['uid'] and not short_uid:
            raise ValueError('Ожидался uid или short_uid')
        return short_uid


@csrf_exempt
@check_authorization()
def user_req(request, **kwargs):
    """Обработка стандартных запросов связанных с пользователями системы.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'GET':
        user_info = {
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'date_joined': user.date_joined,
        }
        return get_response(True, 200, True, '', user_info=user_info, )

    elif request.method == 'PUT':
        req_data = UserNewData.parse_raw(request.body)
        user.first_name = req_data.first_name
        user.last_name = req_data.last_name
        user.email = req_data.email
        user.save()
        return get_response(True, 200, True, '')

    else:
        return get_response(True, 405, False, ERR_LIST['no_method'])


@csrf_exempt
@check_authorization()
def change_password(request, **kwargs):
    """Обработка запроса смены пароля.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'PUT':
        req_data = NewPwdData.parse_raw(request.body)
        if not user.check_password(req_data.old_password):
            return get_response(True, 403, False, 'Старый пароль неверен!')
        user.set_password(req_data.new_password_first)
        user.save()
        return get_response(True, 200, True, '')

    else:
        return get_response(True, 405, False, ERR_LIST['no_method'])


@csrf_exempt
@check_authorization()
def users_cube(request, **kwargs):
    """Обработка запросов для пользовательских кубов.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'GET':
        users_cube_list = Cube.objects.filter(all_users=user)
        cube_info = []
        for cube in users_cube_list:
            cube_info.append(cube.short_info())
        return get_response(True, 200, True, '', cube_info=cube_info)
    if request.method == 'POST':
        req_data = AppendCubeData.parse_raw(request.body)
        cube = None
        if req_data.short_uid:
            cube = Cube.objects.filter(
                short_uid=req_data.short_uid,
                activation_date=None,
            ).first()
        elif req_data.uid:
            cube = Cube.objects.filter(
                uid=req_data.uid,
                activation_date=None,
            ).first()
        if not cube:
            return get_response(True, 400, False, 'Устройство не найдено!')
        cube.all_users.add(user)
        cube.main_user = user
        cube.activation_date = timezone.now()
        cube.save()
        return get_response(True, 200, True, '', cube_id=cube.pk)
    else:
        return get_response(True, 405, False, ERR_LIST['no_method'])
