"""Модуль обработки запросов связанных с датчиками шума."""

# 127.0.0.1:8000/api/cube/noise_sensor/report
from datetime import datetime

from django.views.decorators.csrf import csrf_exempt
from pydantic import BaseModel

from backend.models import Cube, NoiseSensor
from backend.modules.auth import check_authorization
from backend.modules.html_parser import render_to_pdf
from backend.modules.request_serialize import get_response


class NoiseSensorData(BaseModel):
    cube_id: int
    device_id: int
    date_start: datetime
    date_end: datetime


@csrf_exempt
@check_authorization()
def report_req(request, **kwargs):
    """Функция принятия стандартных запросов связанных с устройствами.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'POST':
        req_data = NoiseSensorData.parse_raw(request.body)
        cube = Cube.objects.filter(pk=req_data.cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        noise_sensor = NoiseSensor.objects.filter(pk=req_data.device_id, cube=req_data.cube_id).first()
        if not noise_sensor:
            return get_response(True, 400, False, 'Датчик шума не найден')
        events_list = noise_sensor.get_history(req_data.date_start, req_data.date_end)
        if not events_list:
            return get_response(True, 400, False, 'За данный период не найдено событий!')
        max_value = 0
        max_value_date = None
        number_of_exceedances = 0
        for event in events_list:
            if event['value'] > noise_sensor.threshold_value:
                number_of_exceedances += 1
                if event['value'] > max_value:
                    max_value = event['value']
                    max_value_date = event['date'].strftime("%d.%m.%Y %H:%M:%S")
        context_dict = {
            'date_start': req_data.date_start.strftime("%d.%m.%Y %H:%M:%S"),
            'date_end': req_data.date_end.strftime("%d.%m.%Y %H:%M:%S"),
            'cub_name': cube.name,
            'device_description': noise_sensor.custom_description,
            'activation_date': noise_sensor.activation_date.strftime("%d.%m.%Y %H:%M:%S"),
            'threshold_value': noise_sensor.threshold_value,
            'events_list': events_list,
            'events_count': number_of_exceedances,
            'max_value': max_value,
            'max_value_date': max_value_date

        }
        return render_to_pdf('pdf/noise_report.html', context_dict, 'report')
    else:
        return get_response(True, 405, False, 'Метод отсутствует')
