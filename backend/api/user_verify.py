import json
import random
import string
import pytz
from datetime import datetime

from backend.models import UserVerify
from backend.modules.auth import check_authorization
from backend.modules.request_serialize import get_response
from config import EMAIL_VERIFY_MESSAGE, EMAIL_VERIFY_SUBJECT
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt
from pydantic import BaseModel
from pydantic.class_validators import validator


class CodeVerify(BaseModel):
    verify_code: str

    @validator('verify_code')
    def check_len_code(cls, verify_code: str):
        if len(verify_code) != 8:
            raise ValueError('Код подтверждения должен состоять из 8 символов')
        return verify_code


class IsAuth(BaseModel):
    is_verify: bool


def generate_random_code():
    letters_and_digits = string.ascii_letters + string.digits
    rand_string = ''.join(random.sample(letters_and_digits, 8))
    return rand_string


@csrf_exempt
@check_authorization()
def send_verify_message(request):
    if request.method == "POST":
        user_access_status = UserVerify.objects.filter(
            user=request.user.id).first()

        if (datetime.utcnow().replace(tzinfo=pytz.UTC) - user_access_status.date_verify).seconds//60 > 5:
            verify_code = generate_random_code()
            send_mail(
                EMAIL_VERIFY_SUBJECT,
                EMAIL_VERIFY_MESSAGE.format(
                    request.user,
                    verify_code,
                ),
                'chel.smartcube@gmail.com',
                [request.user.email],
            )
            user_access_status.date_verify = datetime.utcnow().replace(tzinfo=pytz.UTC)
            if user_access_status:
                user_access_status.verify_code = verify_code
                user_access_status.is_auth = False
                user_access_status.save()
            else:
                user_access_status = UserVerify(user_id=request.user.id)
                user_access_status.verify_code = verify_code
                user_access_status.save()
        return get_response(True, 200, True, '')


@csrf_exempt
@check_authorization()
def confirm_verify(request, **kwargs):
    if request.method == "GET":
        # Получение информации о статусе двойной аутентификации в пользователя
        user = kwargs.get('user')
        user_access_status = UserVerify.objects.filter(
            user_id=user.id).first()
        if not user_access_status:
            return get_response(True, 400, False, 'Пользователь остутсвует в базе')
        return get_response(True, 200, True, '', user_verify=user_access_status.is_connected)

    if request.method == "POST":
        # Проверка кода подтверждения введенного пользователем
        req_data = CodeVerify.parse_raw(request.body)
        user_access_status = UserVerify.objects.filter(
            user_id=request.user.id, verify_code=req_data.verify_code).first()
        if not user_access_status:
            return get_response(True, 400, True, 'Неправильный код подтверждения')
        user_access_status.is_auth = True
        user_access_status.save()
        return get_response(True, 200, True, '')

    if request.method == "PUT":
        # Изменение значения атрибута флага активности двухфакторной аутентификации
        req_data = IsAuth.parse_raw(request.body)
        user_auth_status = UserVerify.objects.filter(
            user_id=request.user.id).first()
        user_auth_status.is_connected = req_data.is_verify
        user_auth_status.save()
        return get_response(True, 200, True, '')
