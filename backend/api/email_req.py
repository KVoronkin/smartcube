"""Модуль для запросов отправки почты."""
import datetime

from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt
from pydantic import BaseModel
from pydantic.networks import EmailStr

from backend.modules.request_serialize import get_response
from config import EMAIL_REVIEW_MESSAGE, EMAIL_REVIEW_SUBJECT


class ReviewData(BaseModel):
    user_name: str
    user_mail: EmailStr
    user_phone: int
    message: str


@csrf_exempt
def review_form(request):
    """Функция принятия запроса отправки отзыва с формы сайта.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    if request.method == 'POST':
        req_body = ReviewData.parse_raw(request.body)
        datetime_now = datetime.datetime.now()
        send_mail(
            EMAIL_REVIEW_SUBJECT,
            EMAIL_REVIEW_MESSAGE.format(
                req_body.user_name,
                req_body.user_phone,
                req_body.user_mail,
                req_body.message,
                datetime_now.strftime("%d-%m-%Y %H:%M")
            ),
            'chel.smartcube@gmail.com',
            ['smartstiller@gmail.com'],
            fail_silently=False,
        )
        return get_response(True, 200, True, '')
