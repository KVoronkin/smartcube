"""Модуль для тестовых запросов."""
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from pydantic import BaseModel, validator

from backend.models import Cube
from backend.modules.auth import check_authorization
from backend.modules.request_serialize import get_response


class UpdateCubeInfo(BaseModel):
    cube_id: int
    name: str
    description: str

    @validator('name')
    def check_name(cls, name):
        if not name:
            raise ValueError('Имя отсутсвует')
        return name


class NewUserForCube(BaseModel):
    cube_id: int
    username: str
    password: str


class DeleteCubeUser(BaseModel):
    cube_id: int
    username: str


@csrf_exempt
@check_authorization()
def cube_req(request, **kwargs):
    """Функция принятия стандартных запросов связанных с кубами.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'GET':
        cube_id = request.GET.get("id", -1)
        cube = Cube.objects.filter(pk=cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        return get_response(True, 200, True, '',
                            cube_info=cube.full_info(),
                            )
    elif request.method == 'PUT':
        req_data = UpdateCubeInfo.parse_raw(request.body)
        cube = Cube.objects.filter(pk=req_data.cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        cube.name = req_data.name
        cube.custom_description = req_data.description
        cube.save(update_fields=['name', 'custom_description'])
        return get_response(True, 200, True, '', id=cube.pk)
    else:
        return get_response(True, 405, False, 'Метод отсутствует')


@csrf_exempt
@check_authorization()
def cube_users(request, **kwargs):
    """Функция принятия запросов связанных с пользователями куба.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'POST':
        req_data = NewUserForCube.parse_raw(request.body)
        cube = Cube.objects.filter(pk=req_data.cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        if cube.main_user != user:
            return get_response(True, 403, False, 'В доступе отказано')
        new_user = User.objects.filter(username=req_data.username).first()
        if not new_user:
            return get_response(True, 400, False, 'Пользователь с таким логином не существует.')
        if not new_user.check_password(req_data.password):
            return get_response(True, 403, False, 'Пароль неверен!')
        if new_user in cube.all_users.all():
            return get_response(True, 400, False, 'Пользователь уже привязан к кубу.')
        cube.all_users.add(new_user)
        cube.save()
        return get_response(True, 200, True, '', id=cube.id)
    elif request.method == 'DELETE':
        req_data = DeleteCubeUser.parse_raw(request.body)
        cube = Cube.objects.filter(pk=req_data.cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        if cube.main_user != user:
            return get_response(True, 403, False, 'В доступе отказано')
        user_for_delete = User.objects.filter(username=req_data.username).first()
        if not user_for_delete:
            return get_response(True, 400, False, 'Пользователь с таким логином не существует.')
        if user_for_delete == user:
            return get_response(True, 400, False, 'Нельзя удалить владельца куба.')
        if user_for_delete not in cube.all_users.all():
            return get_response(True, 400, False, 'Пользователь не привязан к кубу. Удаление невозможно.')
        cube.all_users.remove(user_for_delete)
        return get_response(True, 200, True, '')
    else:
        return get_response(True, 405, False, 'Метод отсутствует')


@csrf_exempt
@check_authorization()
def device_req(request, **kwargs):
    """Функция принятия стандартных запросов связанных с кубами.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'GET':
        cube_id = request.GET.get("id", -1)
        cube = Cube.objects.filter(pk=cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        return get_response(True, 200, True, '',
                            device_info=cube.all_device_info()
                            )
    else:
        return get_response(True, 405, False, 'Метод отсутствует')
