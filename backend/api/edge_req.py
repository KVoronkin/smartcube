"""Модуль для запросов связанных к гранями куба."""
from django.views.decorators.csrf import csrf_exempt
from pydantic import BaseModel

from backend.enums.device_types import DeviceTypes
from backend.models import Cube, Device, CubeEdge
from backend.modules.auth import check_authorization
from backend.modules.request_serialize import get_response


class AddDeviceToEdgeData(BaseModel):
    cube_id: int
    device_id: int
    edge_id: int


@csrf_exempt
@check_authorization()
def edge_req(request, **kwargs):
    """Функция принятия стандартных запросов связанных с кубами.

    Args:
        request: Данные поступившего запроса.

    Returns:
        Ответ сервера.
    """
    user = kwargs.get('user')
    if request.method == 'PUT':
        req_data = AddDeviceToEdgeData.parse_raw(request.body)
        cube = Cube.objects.filter(pk=req_data.cube_id, all_users=user).first()
        if not cube:
            return get_response(True, 400, False, 'Куб не найден')
        device = Device.objects.filter(pk=req_data.device_id, cube=cube).first()
        if not device:
            return get_response(True, 400, False, 'Устройство не найдено')
        edge = CubeEdge.objects.filter(pk=req_data.edge_id).first()
        if not edge:
            return get_response(True, 400, False, 'Грань не найдена')
        if edge not in cube.all_edge():
            return get_response(True, 400, False, 'Грань не принадлежит данному кубу.')
        if kwargs.get('action') == 'button':
            edge.button.device = device
            edge.button.save()
            return get_response(True, 200, True, '', id=edge.pk)
        if device.device_type not in DeviceTypes.device_to_edge_types():
            return get_response(True, 400, False, 'Невозможно привязать данное устройство к грани.')
        edge.device = device
        edge.save()
        return get_response(True, 200, True, '', id=edge.pk)
    elif request.method == 'GET':
        if kwargs.get('action') == 'device_types':
            return get_response(True, 200, True, '', device_types=DeviceTypes.device_to_edge_types())
        else:
            return get_response(True, 404, False, 'Страница не найдена')
    else:
        return get_response(True, 405, False, 'Метод отсутствует')
