""""Модуль создания моделей ORM Django."""
# -*- coding: UTF-8 -*-
from datetime import datetime

from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone
from model_utils.managers import InheritanceManager

from backend.enums.device_types import DeviceTypes
from backend.modules.code_generate import generate_uid


class EdgeButton(models.Model):
    """Модель хранения информации о всех кнопках на грани куба."""

    is_pressed = models.BooleanField(
        default=False,
        verbose_name='Нажата'
    )
    device = models.ForeignKey(
        "backend.Device",
        verbose_name='Привязанное устройство',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    def full_info(self):
        button_info = {
            'id': self.pk,
            'is_pressed': self.is_pressed,
            'device_info': {}
        }
        if self.device:
            device = Device.objects.get_subclass(pk=self.device.pk)
            button_info['device_info'] = device.full_info()
        return button_info

    class Meta:
        verbose_name = 'Кнопка грани'
        verbose_name_plural = 'Кнопки граней'

    def __str__(self):
        return str(self.pk)


class CubeEdge(models.Model):
    """Модель хранения информации о всех гранях куба."""

    color = models.CharField(max_length=50, verbose_name='Цвет грани (HEX)')
    button = models.OneToOneField(
        EdgeButton,
        verbose_name='Кнопка',
        on_delete=models.CASCADE,
    )
    device = models.ForeignKey(
        "backend.Device",
        verbose_name='Привязанное устройство',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    def full_info(self):
        edge_info = {
            'id': self.pk,
            'color': self.color,
            'button_info': self.button.full_info(),
            'device_info': {},
        }
        if self.device:
            device = Device.objects.get_subclass(pk=self.device.pk)
            edge_info['device_info'] = device.full_info()
        return edge_info

    class Meta:
        verbose_name = 'Грань куба'
        verbose_name_plural = 'Грани куба'

    def __str__(self):
        return self.color


class Cube(models.Model):
    """Модель хранения информации о всех устройствах."""

    name = models.CharField(
        max_length=50,
        verbose_name='Название',
        default='Куб'
    )
    uid = models.CharField(
        max_length=50,
        verbose_name='UID',
        default=generate_uid,
    )
    short_uid = models.CharField(
        max_length=50,
        verbose_name='Сокращенный UID (PIN)',
        default='short',
    )
    all_users = models.ManyToManyField(
        User,
        verbose_name='Допольнительные пользователи',
        blank=True,
        default=None,
        related_name='all_users',
    )
    # TODO: Автоматическое добавление main_user
    main_user = models.ForeignKey(
        User,
        verbose_name='Владелец',
        blank=True,
        null=True,
        default=None,
        on_delete=models.SET_NULL,
        related_name='main_user',
    )
    custom_description = models.CharField(
        max_length=200,
        verbose_name='Дополнительное описание',
        blank=True,
        null=True,
        default=None
    )
    activation_date = models.DateTimeField(
        'Дата активации',
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True,
        default=None,
    )
    first_edge = models.OneToOneField(
        CubeEdge,
        verbose_name='Первая грань',
        on_delete=models.CASCADE,
        related_name='first_edge',
        default=None,
    )
    second_edge = models.OneToOneField(
        CubeEdge,
        verbose_name='Вторая грань',
        on_delete=models.CASCADE,
        related_name='second_edge',
        default=None,
    )
    third_edge = models.OneToOneField(
        CubeEdge,
        verbose_name='Третья грань',
        on_delete=models.CASCADE,
        related_name='third_edge',
        default=None,
    )
    fourth_edge = models.OneToOneField(
        CubeEdge,
        verbose_name='Четвертая грань',
        on_delete=models.CASCADE,
        related_name='fourth_edge',
        default=None,
    )
    fifth_edge = models.OneToOneField(
        CubeEdge,
        verbose_name='Пятая грань',
        on_delete=models.CASCADE,
        related_name='fifth_edge',
        default=None,
    )

    def save(self, *args, **kwargs):
        """Переопределение метода сохранения в базу."""
        self.short_uid = self.uid[:8]
        super(Cube, self).save(*args, **kwargs)

    def all_device(self):
        """Вернуть все устройства в виде выборки объектов из БД.

        @return: QuerySet: Все устройства куба.
        """
        devices = Device.objects.filter(cube=self)
        return devices

    def all_edge(self):
        """Верунть список всех граней куба в виде списка объектов из БД.

        @return: list: Все грани куба.
        """
        return [
            self.first_edge,
            self.second_edge,
            self.third_edge,
            self.fourth_edge,
            self.fifth_edge,
        ]

    def short_info(self):
        """Получить краткую информацию о устройстве.

        Returns:
            dict: Информация о устройстве.
        """
        device_info = {
            'id': self.pk,
            'name': self.name,
            'custom_description': self.custom_description,
        }
        return device_info

    def all_device_info(self):
        """Получить информацию о всех датчиках для куба.

        Returns:
            dict: Информация о датчиках.
        """
        all_devices = {
            'noise_sensor': [],
            'temperature_sensors': [],
            'humidity_sensors': [],
            'opening_sensors': [],
            'motion_sensors': [],
        }
        devices = Device.objects.filter(cube=self)
        for device in devices:
            if device.device_type == DeviceTypes.NOISE_SENSOR.value:
                all_devices['noise_sensor'].append(device.short_info())
            if device.device_type == DeviceTypes.TEMPERATURE_SENSOR.value:
                all_devices['temperature_sensors'].append(
                    device.short_info())
            if device.device_type == DeviceTypes.HUMIDITY_SENSOR.value:
                all_devices['humidity_sensors'].append(device.short_info())
            if device.device_type == DeviceTypes.OPENING_SENSOR.value:
                all_devices['opening_sensors'].append(device.short_info())
            if device.device_type == DeviceTypes.MOTION_SENSOR.value:
                all_devices['motion_sensors'].append(device.short_info())
        return all_devices

    def full_info(self):
        """Получить полную информацию.

        Returns: dict: Информация о кубе.
        """
        device_info = {
            'id': self.pk,
            'name': self.name,
            'custom_description': self.custom_description,
            'activation_date': self.activation_date,
            'users': list(self.all_users.all().values_list('username',flat=True)),
            'first_edge': self.first_edge.full_info(),
            'second_edge': self.second_edge.full_info(),
            'third_edge': self.third_edge.full_info(),
            'fourth_edge': self.fourth_edge.full_info(),
            'fifth_edge': self.fifth_edge.full_info(),
        }
        return device_info

    class Meta:
        verbose_name = 'Устройство (Куб)'
        verbose_name_plural = 'Устройства (Кубы)'

    def __str__(self):
        return self.name


class Device(models.Model):
    """Абстрактная модель хранения информации о устройствах."""

    cube = models.ForeignKey(
        Cube,
        verbose_name='Куб',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
    )
    custom_description = models.CharField(
        max_length=200,
        verbose_name='Дополнительное описание',
        blank=True,
        null=True,
        default=None
    )
    uid = models.CharField(
        max_length=50,
        verbose_name='UID',
        default=generate_uid,
    )
    short_uid = models.CharField(
        max_length=50,
        verbose_name='Сокращенный UID (PIN)',
        default='short',
    )
    activation_date = models.DateTimeField(
        'Дата активации',
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True,
        default=None,
    )
    is_enabled = models.BooleanField(
        default=False,
        verbose_name='Работает'
    )
    device_type = models.IntegerField(
        'Тип устройства',
        default=0,
    )
    objects = InheritanceManager()

    def save(self, *args, **kwargs):
        self.short_uid = self.uid[:8]
        super(Device, self).save(*args, **kwargs)

    def short_info(self):
        info = {
            'id': self.id,
            'is_enabled': self.is_enabled,
            'device_type': self.device_type,
            'description': self.custom_description,
            'activation_date': self.activation_date,
        }
        return info

    def full_info(self):
        return self.short_info()

    def get_history(self, date_start: datetime, date_end: datetime):
        return []


class NoiseSensor(Device):
    """Модель хранения информации о всех датчиках шума."""

    threshold_value = models.IntegerField('Пороговое значение', default=55)

    def __init__(self, *args, **kwargs):
        kwargs['device_type'] = DeviceTypes.NOISE_SENSOR.value
        Device.__init__(self, *args, **kwargs)

    class Meta:
        verbose_name = 'Датчик шума'
        verbose_name_plural = 'Датчики шума'

    def full_info(self):
        info = self.short_info()
        info['threshold_value'] = self.threshold_value
        return info

    def get_history(self, date_start: datetime, date_end: datetime):
        values = NoiseSensorValues.objects.order_by('date').filter(
            noise_sensor=self,
            date__range=(date_start, date_end)
        )
        return list(values.values('value', 'date'))


class NoiseSensorValues(models.Model):
    """Модель хранения информации о значениях всех датчиков шума."""

    noise_sensor = models.ForeignKey(
        NoiseSensor,
        verbose_name='Датчик шума',
        on_delete=models.CASCADE,
    )
    value = models.FloatField(
        verbose_name='Значение'
    )
    date = models.DateTimeField(
        verbose_name='Время сработки',
        default=timezone.now
    )

    class Meta:
        verbose_name = 'Значения датчика шума'
        verbose_name_plural = 'Значения датчиков шума'


class TemperatureSensor(Device):
    """Модель хранения информации о всех датчиках температуры."""

    min_value = models.IntegerField('Предельный минимум', default=15)
    max_value = models.IntegerField('Предельный максимум', default=30)

    def __init__(self, *args, **kwargs):
        kwargs['device_type'] = DeviceTypes.TEMPERATURE_SENSOR.value
        Device.__init__(self, *args, **kwargs)

    def full_info(self):
        info = self.short_info()
        info['min_value'] = self.min_value
        info['max_value'] = self.min_value
        return info

    def get_history(self, date_start: datetime, date_end: datetime):
        values = TemperatureSensorValues.objects.order_by('date').filter(
            temperature_sensor=self,
            date__range=(date_start, date_end)
        )
        return list(values.values('value', 'date'))

    class Meta:
        verbose_name = 'Датчик температуры'
        verbose_name_plural = 'Датчики температуры'


class TemperatureSensorValues(models.Model):
    """Модель хранения информации о значениях всех датчиков температуры."""

    temperature_sensor = models.ForeignKey(
        TemperatureSensor,
        verbose_name='Датчик температуры',
        on_delete=models.CASCADE,
    )
    value = models.FloatField(
        verbose_name='Значение'
    )
    date = models.DateTimeField(
        verbose_name='Время сработки',
        default=timezone.now
    )

    class Meta:
        verbose_name = 'Значения датчика температуры'
        verbose_name_plural = 'Значения датчиков температуры'


class HumiditySensor(Device):
    """Модель хранения информации о всех датчиках влажности."""

    threshold_value = models.IntegerField('Пороговое значение', default=40)

    def __init__(self, *args, **kwargs):
        kwargs['device_type'] = DeviceTypes.HUMIDITY_SENSOR.value
        Device.__init__(self, *args, **kwargs)

    def full_info(self):
        info = self.short_info()
        info['threshold_value'] = self.threshold_value
        return info

    def get_history(self, date_start: datetime, date_end: datetime):
        values = HumiditySensorValues.objects.order_by('date').filter(
            humidity_sensor=self,
            date__range=(date_start, date_end)
        )
        return list(values.values('value', 'date'))

    class Meta:
        verbose_name = 'Датчик влажности'
        verbose_name_plural = 'Датчики влажности'


class HumiditySensorValues(models.Model):
    """Модель хранения информации о значениях всех датчиков влажности."""

    humidity_sensor = models.ForeignKey(
        HumiditySensor,
        verbose_name='Датчик влажности',
        on_delete=models.CASCADE,
    )
    value = models.FloatField(
        verbose_name='Значение'
    )
    date = models.DateTimeField(
        verbose_name='Время сработки',
        default=timezone.now
    )

    class Meta:
        verbose_name = 'Значения датчика влажности'
        verbose_name_plural = 'Значения датчиков влажности'


class MotionSensor(Device):
    """Модель хранения информации о всех датчиках движения."""

    def __init__(self, *args, **kwargs):
        kwargs['device_type'] = DeviceTypes.MOTION_SENSOR.value
        Device.__init__(self, *args, **kwargs)

    class Meta:
        verbose_name = 'Датчик движения'
        verbose_name_plural = 'Датчики движения'

    def get_history(self, date_start: datetime, date_end: datetime):
        values = MotionSensorValues.objects.order_by('date').filter(
            motion_sensor=self,
            date__range=(date_start, date_end)
        )
        return list(values.values('value', 'date'))


class MotionSensorValues(models.Model):
    """Модель хранения информации о значениях всех датчиков движения."""

    motion_sensor = models.ForeignKey(
        MotionSensor,
        verbose_name='Датчик движения',
        on_delete=models.CASCADE,
    )
    value = models.BooleanField(
        verbose_name='Значение',
    )
    date = models.DateTimeField(
        verbose_name='Время сработки',
        default=timezone.now,
    )

    class Meta:
        verbose_name = 'Значения датчика движения'
        verbose_name_plural = 'Значения датчиков движения'


class OpeningSensor(Device):
    """Модель хранения информации о всех датчиках открытия."""

    def __init__(self, *args, **kwargs):
        kwargs['device_type'] = DeviceTypes.OPENING_SENSOR.value
        Device.__init__(self, *args, **kwargs)

    class Meta:
        verbose_name = 'Датчик открытия'
        verbose_name_plural = 'Датчики открытия'

    def get_history(self, date_start: datetime, date_end: datetime):
        values = OpeningSensorValues.objects.order_by('date').filter(
            opening_sensor=self,
            date__range=(date_start, date_end)
        )
        values.order_by('date')
        return list(values.values('value', 'date'))


class OpeningSensorValues(models.Model):
    """Модель хранения информации о значениях всех датчиков открытия."""

    opening_sensor = models.ForeignKey(
        OpeningSensor,
        verbose_name='Датчик открытия',
        on_delete=models.CASCADE,
    )
    value = models.BooleanField(
        verbose_name='Значение',
    )
    date = models.DateTimeField(
        verbose_name='Время сработки',
        default=timezone.now,
    )

    class Meta:
        verbose_name = 'Значения датчика открытия'
        verbose_name_plural = 'Значения датчиков открытия'


class SmartSocket(Device):
    """Модель хранения информации о всех умных розетках."""

    def __init__(self, *args, **kwargs):
        kwargs['device_type'] = DeviceTypes.SMART_SOCKET.value
        Device.__init__(self, *args, **kwargs)

    class Meta:
        verbose_name = 'Умная розетка'
        verbose_name_plural = 'Умные розетки'


class SmartCurtain(Device):
    """Модель хранения информации о всех умных шторах."""
    value = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(100)])

    def __init__(self, *args, **kwargs):
        kwargs['device_type'] = DeviceTypes.CURTAIN.value
        Device.__init__(self, *args, **kwargs)

    class Meta:
        verbose_name = 'Умная штора'
        verbose_name_plural = 'Умные шторы'


class SmartLamp(Device):
    """Модель хранения информации о всех умных лампах."""
    value = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(100)])

    def __init__(self, *args, **kwargs):
        kwargs['device_type'] = DeviceTypes.LAMP.value
        Device.__init__(self, *args, **kwargs)

    class Meta:
        verbose_name = 'Умная лампа'
        verbose_name_plural = 'Умные лампы'

# Таблица двойной аутентификации
class UserVerify(models.Model):
    """Модель хранения о духфактороной аутентификации пользователя."""

    is_auth = models.BooleanField(
        verbose_name='Пройдена ли аунтентификация',
        default=False
    )
    verify_code = models.CharField(
        max_length=8,
        verbose_name='Code',
        default=None,
    )
    is_connected = models.BooleanField(
        verbose_name='Включена ли двухфакторная аутентификация',
        default=True,
    )
    user = models.OneToOneField(
        User,
        on_delete = models.CASCADE, # Связанное удаление
        primary_key = True,
    )
    date_verify = models.DateTimeField(
        verbose_name='Время отправления кода подтверждения',
        default=timezone.now
    )
