"""Модуль с кастомными middleware."""
from .modules.request_serialize import get_response


class DeserializeJsonMiddleware:
    """Middleware отлова и обработки ошибок валидации."""
    def __init__(self, get_response):
        self._get_response = get_response

    def __call__(self, request):
        response = self._get_response(request)
        return response

    def process_exception(self, requset, Error):
        if (str(type(Error)) == """<class 'pydantic.error_wrappers.ValidationError'>"""):
            return get_response(
                True, 400, False, 'Bad request',
                datails=Error.errors()
            )
