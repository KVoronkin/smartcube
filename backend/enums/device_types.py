"""Enum возможных типов устройств."""
import enum


class DeviceTypes(enum.Enum):
    """Перечисление возможных типов утройств."""

    NOISE_SENSOR = 0
    TEMPERATURE_SENSOR = 1
    HUMIDITY_SENSOR = 2
    MOTION_SENSOR = 3
    OPENING_SENSOR = 4
    SMART_SOCKET = 5

    LAMP = 6
    CURTAIN = 7

    @staticmethod
    def value_to_name(value):
        names = {
            0: 'noise_sensor',
            1: 'temperature_sensor',
            2: 'humidity_sensor',
            3: 'opening_sensor',
            4: 'opening_sensor',
            5: 'smart_socket',
            6: 'lamp',
            7: 'curtain',
        }
        return names.get(value)

    @classmethod
    def device_with_threshold_value(cls):
        return (
            cls.NOISE_SENSOR.value,
            cls.HUMIDITY_SENSOR.value,
        )

    @classmethod
    def device_to_edge_types(cls):
        """Вернуть типы датчиков, которые можно привязать к граням.

        Returns:
            [type]: [description]
        """
        return [
            cls.LAMP.value,
            cls.CURTAIN.value
        ]
