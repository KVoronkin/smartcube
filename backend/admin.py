"""Модуль настройки административной панели Django."""
from django.contrib import admin

from . import models


class CubeAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели Device."""

    list_display = (
        'name',
        'uid',
        'short_uid',
        'all_users_list',
        'main_user',
        'activation_date',
        'first_edge',
        'second_edge',
        'third_edge',
        'fourth_edge',
        'fifth_edge',
    )
    readonly_fields = ('uid', 'short_uid')

    def all_users_list(self, obj):
        return "\n".join([user.username for user in obj.all_users.all()])


class CubeEdgeAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели DeviceEdge."""

    list_display = (
        'color',
        'button',
        'device',
    )


class EdgeButtonAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели Device."""

    list_display = (
        'pk',
        'is_pressed',
        'device',
    )


class NoiseSensorAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели NoiseSensor."""
    exclude = ['device_type']
    list_display = (
        'cube',
        'uid',
        'custom_description',
        'short_uid',
        'activation_date',
        'threshold_value',
    )
    readonly_fields = ('uid', 'short_uid')


class NoiseSensorValuesAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели NoizeSensorValues."""

    list_display = (
        'noise_sensor',
        'value',
        'date',
    )


class TemperatureSensorAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели TemperatureSensor."""
    exclude = ['device_type']
    list_display = (
        'cube',
        'uid',
        'short_uid',
        'custom_description',
        'activation_date',
        'min_value',
        'max_value',
    )
    readonly_fields = ('uid', 'short_uid')


class TemperatureSensorValuesAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели TemperatureSensorValues."""

    list_display = (
        'temperature_sensor',
        'value',
        'date',
    )


class HumiditySensorAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели HumiditySensor."""
    exclude = ['device_type']
    list_display = (
        'cube',
        'uid',
        'short_uid',
        'custom_description',
        'activation_date',
        'threshold_value',
    )
    readonly_fields = ('uid', 'short_uid')


class HumiditySensorValuesAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели HumiditySensorValues."""

    list_display = (
        'humidity_sensor',
        'value',
        'date',
    )


class MotionSensorAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели MotionSensor."""
    exclude = ['device_type']
    list_display = (
        'cube',
        'uid',
        'short_uid',
        'custom_description',
        'activation_date',
    )
    readonly_fields = ('uid', 'short_uid')


class MotionSensorValuesAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели MotionSensorValues."""

    list_display = (
        'motion_sensor',
        'value',
        'date',
    )


class OpeningSensorAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели OpeningSensor."""
    exclude = ['device_type']
    list_display = (
        'cube',
        'uid',
        'short_uid',
        'custom_description',
        'activation_date',
    )
    readonly_fields = ('uid', 'short_uid')


class OpeningSensorValuesAdmin(admin.ModelAdmin):
    """Класс нрастройки визуального представления модели OpeningSensorValues."""

    list_display = (
        'opening_sensor',
        'value',
        'date',
    )


admin.site.register(models.Cube, CubeAdmin)
admin.site.register(models.NoiseSensor, NoiseSensorAdmin)
admin.site.register(models.NoiseSensorValues, NoiseSensorValuesAdmin)
admin.site.register(models.TemperatureSensor, TemperatureSensorAdmin)
admin.site.register(models.TemperatureSensorValues,
                    TemperatureSensorValuesAdmin)
admin.site.register(models.HumiditySensor, HumiditySensorAdmin)
admin.site.register(models.HumiditySensorValues, HumiditySensorValuesAdmin)
admin.site.register(models.MotionSensor, MotionSensorAdmin)
admin.site.register(models.MotionSensorValues, MotionSensorValuesAdmin)
admin.site.register(models.OpeningSensor, OpeningSensorAdmin)
admin.site.register(models.OpeningSensorValues, OpeningSensorValuesAdmin)
admin.site.register(models.CubeEdge, CubeEdgeAdmin)
admin.site.register(models.EdgeButton, EdgeButtonAdmin)
