# Generated by Django 3.1.7 on 2021-04-21 15:16

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SmartCurtain',
            fields=[
                ('device_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='backend.device')),
                ('value', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)])),
            ],
            options={
                'verbose_name': 'Умная штора',
                'verbose_name_plural': 'Умные шторы',
            },
            bases=('backend.device',),
        ),
        migrations.CreateModel(
            name='SmartLamp',
            fields=[
                ('device_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='backend.device')),
                ('value', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)])),
            ],
            options={
                'verbose_name': 'Умная лампа',
                'verbose_name_plural': 'Умные лампы',
            },
            bases=('backend.device',),
        ),
        migrations.CreateModel(
            name='SmartSocket',
            fields=[
                ('device_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='backend.device')),
            ],
            options={
                'verbose_name': 'Умная розетка',
                'verbose_name_plural': 'Умные розетки',
            },
            bases=('backend.device',),
        ),
    ]
