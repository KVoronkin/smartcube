from django.shortcuts import render, redirect
from backend.models import UserVerify
from config import SSH_ROOT
import requests
from django.db.models import Q

def cart(request):
    """Шаблон для корзины пользователя"""
    return render(request, 'CubeViewer/Cart.html')

def order(request):
    """Шаблон для заполнения документа на покупку товара"""
    return render(request, 'CubeViewer/MakeOrder.html')

def salepage(request):
    """Шаблон главной страницы сайта"""
    return render(request, 'CubeViewer/SalePage.html')

def mycab(request):
    """Шаблон личного кабинета пользователя"""
    # Проверка на прохождение двухфакторной аутентификации
    if not UserVerify.objects.filter(Q(
        is_connected=True,
        is_auth=True) | Q(is_connected=False)
    ).filter(user_id=request.user.id).exists():
        return redirect('/accounts/verify/')

    # Сбор данных о кубах и устройствах пользователя и текущем выбранном кубе
    if request.user.is_authenticated:
        user_cubes_list = requests.get('http://{0}/api/user/cube'.format(SSH_ROOT),
                                       headers={
                                           "content-type": "application/json",
                                           "Authorization": request.COOKIES.get('sessionid')
        }).json().get('cube_info')
        selected_cube = request.GET.get("cube")
        selected_cube_info = {'id': 0, 'name': 'У вас еще нет куба'}
        if not selected_cube and user_cubes_list:
            selected_cube = user_cubes_list[0].get('id')
        if selected_cube is not None:
            selected_cube_info = requests.get('http://{0}/api/cube/devices_info?id={1}'.format(SSH_ROOT, selected_cube),
                                              headers={
                "content-type": "application/json",
                "Authorization": request.COOKIES.get('sessionid')
            }).json()
        return render(request, 'CubeViewer/Cabinet.html', {
            'selected_cube': selected_cube,
            'cub_list': user_cubes_list,
            'selected_cube_info': selected_cube_info.get('device_info'),
        })
    else:
        return redirect('/accounts/login')

def settings(request):
    """Шаблон страницы настроек"""
    # Проверка на прохождение двухфакторной аутентификации
    if not UserVerify.objects.filter(Q(
        is_connected=True,
        is_auth=True) | Q(is_connected=False)
    ).filter(user_id=request.user.id).exists():
        return redirect('/accounts/verify/')

    # Сбор данных о кубах пользователя и текущем выбранном кубе
    user_cubes_list = None
    selected_tab = request.GET.get('tab')
    if selected_tab is None:
        selected_tab = 'Cubes'
    if selected_tab != "Profile":
        user_cubes_list = requests.get('http://{0}/api/user/cube'.format(SSH_ROOT),
                                        headers={
            "content-type": "application/json",
            "Authorization": request.COOKIES.get('sessionid')
        }).json().get('cube_info')
    start_cube = {
        "id": 0,
        "name": 'Выберите куб',
    }
    if user_cubes_list is not None:
        start_cube = user_cubes_list[0]
    is_auth_connected = requests.get('http://{0}/api/user/verify'.format(SSH_ROOT),
                                     headers={
        "content-type": "application/json",
        "Authorization": request.COOKIES.get('sessionid')
    }).json().get('user_verify')
    return render(request, 'CubeViewer/settings.html', {
        'tab': selected_tab,
        'cubes_list': user_cubes_list,
        'start_cube': start_cube,
        'is_verify': is_auth_connected,
    })

def contacts(request):
    """Шаблон страницы контактов организации"""
    return render(request, 'CubeViewer/contacts.html')

def register(request):
    """Шаблон страницы регистрации"""
    if request.method == "GET":
        return render(request, 'registration/registration.html')

def verify(request):
    """Шаблон страницы подтверждения двухфакторной аутентификации"""
    requests.post('http://{0}/api/user/confirm'.format(SSH_ROOT), headers={
        "content-type": "application/json",
        "Authorization": request.COOKIES.get('sessionid')
    })
    return render(request, 'registration/verify.html')
