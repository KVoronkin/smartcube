from django.urls import path
from . import views

"""Адреса страниц сайта"""
urlpatterns = [
     path('', views.salepage, name='salepage'),
     path('my/', views.mycab, name='mycabinet'),
     path('my/settings/', views.settings, name='settings'),
     path('my/contacts/', views.contacts, name='contacts'),
     path('accounts/registration/', views.register, name='registration'),
     path('accounts/verify/', views.verify, name='verify'),
     path('my/cart/', views.cart, name='cart'),
     path('my/cart/order', views.order, name='order'),
]